const moment = require('moment')
module.exports = {
    onError: function (err) {
        console.clear() // clear terminal
        console.log(`[\x1b[31m${moment().format('HH:mm:ss')}\x1b[0m]\x1b[31m : ERROR OCCURED\x1b[0m`)
        console.log(`[\x1b[41mERROR MESSAGE\x1b[0m]\x1b[33m : ${err.message}\x1b[0m\n`)
        process.exit(0)
    },
    clg: function (log) {
        return console.log(`[\x1b[34m${moment().format('HH:mm:ss')}\x1b[0m]\x1b[36m : \x1b[32m${log}\x1b[0m`)
    },
    list: function (log) {
        return console.log(`           \x1b[36m>\x1b[0m \x1b[2m${log}\x1b[0m`)
    }
}