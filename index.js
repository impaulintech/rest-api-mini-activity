const express = require('express')
const mongoose = require('mongoose')
const subscribersRouter = require('./routes/subscribers')
const func = require('./functions') // Import functions
const app = express()
const port = 3000

try {
    app.use(express.json())
    app.listen(port, () => {
        console.clear() // clear terminal
        //Server Started
        console.log(`\n\x1b[35m=====================\x1b[0m[\x1b[36m IMPAULINTECH \x1b[0m]\x1b[35m=====================\x1b[0m\n`)
        func.clg('Server Started')
        func.list(`Port : ${port}`)
    })
    app.use('/subscribers', subscribersRouter)

    //Database Connection
    const DATABASE_URL = 'mongodb+srv://paulintech:asd123@s30.ra1ml.mongodb.net/test?authSource=admin&replicaSet=atlas-ojl7my-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true' //Authentication
    const db = mongoose.connection

    mongoose.connect(DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true })

    db.on('error', (err) => { func.onError(err) })
    db.once('open', () => {
        // Connected to Database 
        func.clg('Connected to Database')
    })

} catch (error) {
    func.onError(error)
} 